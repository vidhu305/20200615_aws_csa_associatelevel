Agenda :

1. Introduction
2. Sharing the Notes

Git :

https://git-scm.com/

IDE :
https://code.visualstudio.com/download

AWS CLI :
https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html


1. VPC : To host a Website/WebApplication 

For example :

    - Customer : codewithckk.com (godaddy.com)

        - Developers : .jar , .ear , .zip etc... 

    AWS Architects :

    Design an Architecture on AWS based on customer requirement :

        - 2 Tier Architecture 

        HA :
            Two Availability zones 
        
        VPC :
            IGW :
                Route Tables : 2 Route Tables i.e. 1 Public & 2 Private 
                    Subnets :  6 Subnets i.e. 2 Public Facing and 4 Private Facing 
                        NACL : Firewall on VPC level (All Subnets) or Each Subnet level 
                            Security Groups : 1. Bastion Server, 2. WebServer, 3 Database etc... 
                                Bastion / Jumpbox : Linux or Windows (Part of Public Subnet)
                                    To Provide internet to Private Subnet : NAT Gateway part of Public Subnet 
                                        Deploy WebApplication on Private Subnet : EC2 Instance using Autoscaling Group 
                                                    - Launch Configuration 
                                                    - AutoScaling Group 
                                            Deploy a Database on Private Subnet : EC2 with Database or AWS RDS
                                                Create a Load Balancer on Public Subnet : 
                                                    Create Route53 Hosted Zone in AWS : codewithckk.com 
                                                        Copy the Name Servers and update part of the Domain Seller i.e. godaddy.com 

                                                            Map Load Balancer in Route53 : Load Balancer Target need to be mapped 

                                                            Validate : http://codewithckk.com 

                                                            CleanUp!
                                                    







RunningNOtes :


Last login: Mon Jun 22 05:50:28 on console
Clouds-MacBook-Pro:~ cloudbinary$ pwd
/Users/cloudbinary
Clouds-MacBook-Pro:~ cloudbinary$ git clone https://gitlab.com/cloudbinary/20200615_aws_csa_associatelevel.git
Cloning into '20200615_aws_csa_associatelevel'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (3/3), done.
Clouds-MacBook-Pro:~ cloudbinary$ 
Clouds-MacBook-Pro:~ cloudbinary$ ls -ld 20200615_aws_csa_associatelevel/
drwxr-xr-x  4 cloudbinary  staff  128 Jun 22 07:13 20200615_aws_csa_associatelevel/
Clouds-MacBook-Pro:~ cloudbinary$ 
Clouds-MacBook-Pro:~ cloudbinary$ ls -lrta 20200615_aws_csa_associatelevel/
total 8
drwxr-xr-x+ 43 cloudbinary  staff  1376 Jun 22 07:13 ..
drwxr-xr-x   4 cloudbinary  staff   128 Jun 22 07:13 .
-rw-r--r--   1 cloudbinary  staff   138 Jun 22 07:13 README.md
drwxr-xr-x  12 cloudbinary  staff   384 Jun 22 07:13 .git
Clouds-MacBook-Pro:~ cloudbinary$ 

