Getting Started with DynamoDB

Basic Concepts in DynamoDB.

Prerequisites - Getting Started Tutorial.

Step 1: Create a Table.

Step 2: Write Data to a Table Using the Console or AWS CLI.

Step 3: Read Data from a Table.

Step 4: Update Data in a Table.

Step 5: Query Data in a Table.

Step 6: Create a Global Secondary Index.

https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/getting-started-step-2.html

aws dynamodb create-table --table-name Music --attribute-definitions AttributeName=Artist,AttributeType=S AttributeName=SongTitle,AttributeType=S --key-schema AttributeName=Artist,KeyType=HASH AttributeName=SongTitle,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --profile rambo
            
aws dynamodb --profile rambo describe-table --table-name Music 
TABLE   2020-07-01T08:52:36.633000+05:30        0       arn:aws:dynamodb:ap-south-1:727203166843:table/Music    27fbdfba-3517-4b0a-b2b1-eef00b456672    Music   0       ACTIVE
ATTRIBUTEDEFINITIONS    Artist  S
ATTRIBUTEDEFINITIONS    SongTitle       S
KEYSCHEMA       Artist  HASH
KEYSCHEMA       SongTitle       RANGE
PROVISIONEDTHROUGHPUT   0       10      5


You write an application using an AWS SDK for your programming language.

Each AWS SDK provides one or more programmatic interfaces for working with DynamoDB. The specific interfaces available depend on which programming language and AWS SDK you use.

The AWS SDK constructs HTTP(S) requests for use with the low-level DynamoDB API.

The AWS SDK sends the request to the DynamoDB endpoint.

DynamoDB executes the request. If the request is successful, DynamoDB returns an HTTP 200 response code (OK). If the request is unsuccessful, DynamoDB returns an HTTP error code and an error message.

The AWS SDK processes the response and propagates it back to your application.

Each of the AWS SDKs provides important services to your application, including the following:

Formatting HTTP(S) requests and serializing request parameters.

Generating a cryptographic signature for each request.

Forwarding requests to a DynamoDB endpoint and receiving responses from DynamoDB.

Extracting the results from those responses.

Implementing basic retry logic in case of errors.

You do not need to write code for any of these tasks.
